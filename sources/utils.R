## ---- tables ----
t_sty <- function(x) {
  x %>%
    kbl() %>%
    kable_styling(bootstrap_options = "striped") %>%
    kable_paper("hover", full_width = F) %>%
    column_spec(1, bold = T) %>%
    scroll_box(width = "100%")
}

## ---- histograms ----
h_sty <- function(xlab) {
  list(
    geom_histogram(aes(y = ..density..), fill = bb),
    geom_density(color = gg, fill = gg, lwd = .5, alpha = .3),
    theme_light(),
    labs(x = xlab, y = "densidad"),
    theme(legend.position = "none")
  )
}

## ---- boxplot-x-variable ----
bp <- function(x) {
  x %>%
    gather() %>%
    mutate(key = factor(key, levels = unique(key))) %>%
    ggplot(
      aes(
        x = key,
        y = value
      )
    ) +
    geom_boxplot(
      fill = bb,
      outlier.color = oo,
      outlier.alpha = .4
    ) +
    stat_summary(
      fun = median,
      geom = "crossbar",
      width = .75,
      fatten = 2.5,
      color = gg2
    ) +
    stat_summary(
      fun = mean,
      geom = "point",
      shape = 18,
      size = 3,
      color = "gray80"
    ) +
    labs(
      x = "",
      y = ""
    ) +
    theme_light() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0, hjust = 1))
  # par(mar = c(8, 4, 2, 2) + 0.1)
  # boxplot(
  #   datasd,
  #   col = bb,
  #   medcol = gg2,
  #   outcol = oo,
  #   las = 2,
  #   cex.names = .3
  # )
  # points(colMeans(datasd), col = "gray80", pch = 18, lwd = 2, cex = 1.5)
}