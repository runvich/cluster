library(tidyverse)
library(tidytext)

tidy_Velasco_annotated = readRDS("tidy_Velasco_annotated.RDS")
tidy_Velasco = tidy_Velasco_annotated %>% 
  filter(!is.na(lemma)) 

tidy_Larrea_annotated = readRDS("tidy_Larrea_annotated.RDS")
tidy_Larrea = tidy_Larrea_annotated %>% 
  filter(!is.na(lemma))

tidy_Freile_annotated = readRDS("tidy_Freile_annotated.RDS")
tidy_Freile = tidy_Freile_annotated %>% 
  filter(!is.na(lemma))

ejemplo_Velasco = unique(tidy_Velasco$doc_id)[1]
# Mismo resultado
# ejemplo_Velasco = tidy_Velasco %>% pull(doc_id) %>% unique()
# ejemplo_velasco = ejemplo_Velasco[1]
ejemplo_Larrea = unique(tidy_Larrea$doc_id)[1]
ejemplo_Freile = unique(tidy_Freile$doc_id)[2]


# Conteo de palabras (frecuencias)

token1_Velasco = tidy_Velasco %>% 
  count(doc_id, upos, lemma, sort = T) %>% 
  group_by(doc_id) %>%
  mutate(total = sum(n)) %>% 
  ungroup()

# token1_Velasco %>% 
#   filter(doc_id==ejemplo_Velasco) %>% 
#   select(doc_id, lemma, upos, n, total) %>% 
#   head()
token1_Velasco %>%
  filter(doc_id==ejemplo_Velasco) %>%
  select(lemma, n, total) %>%
  head()

token1_Larrea = tidy_Larrea %>% 
  count(doc_id, upos, lemma, sort = TRUE) %>% 
  group_by(doc_id) %>%
  mutate(total = sum(n))

token1_Larrea %>% 
  filter(doc_id==ejemplo_Larrea) %>% 
  select(doc_id, lemma, upos, n, total) %>% 
  head()

token1_Freile = tidy_Freile %>% 
  count(doc_id, upos, lemma, sort = TRUE) %>% 
  group_by(doc_id) %>%
  mutate(total = sum(n))

token1_Freile %>% 
  filter(doc_id==ejemplo_Freile) %>% 
  select(doc_id, lemma, upos, n, total) %>% 
  head()

# Gráfico de frecuencias relativas

token1_Velasco %>% 
  filter(doc_id %in% unique(token1_Velasco$doc_id)[1:4]) %>% 
  filter(upos %in% c('NOUN','PROPN','VERB','ADJ')) %>% 
  group_by(doc_id, upos) %>% 
  slice_max(order_by = n/total, n=3) %>% 
  ggplot(aes(x=reorder_within(lemma, n/total, list(doc_id,upos)), y=n/total, fill = upos)) +
  geom_col() +
  scale_x_reordered()+ # arreglo de etiquetas en eje x
  coord_flip()+
  facet_wrap(vars(doc_id), scales = "free") + 
  labs(x="Frecuencia relativa de cada palabra en cada documento", y="Frecuencia",
       title = "Frecuencias relativas: Cuatro noticias de Juan Fernando Velasco")+
  theme(legend.position = "bottom")

# Tf- Idf
library(tidytext)
token1_Velasco = token1_Velasco %>%
  filter(upos %in% c('NOUN','PROPN','VERB','ADJ')) %>% 
  bind_tf_idf(lemma, doc_id, n)

# token1_Velasco %>%
#   arrange(desc(tf-idf)) %>% 
#   # select(-doc_id) %>% 
#   head() %>% 
#   View()

token1_Velasco %>%
  select(-total) %>%
  arrange(desc(tf_idf))

token1_Larrea = token1_Larrea %>%
  filter(upos %in% c('NOUN','PROPN','VERB','ADJ','X')) %>% 
  bind_tf_idf(lemma, doc_id,n)

token1_Larrea %>% 
  arrange(desc(tf_idf)) %>% 
  head() %>% 
  View()

token1_Larrea %>% 
  filter(upos =="X") %>% # etiqueta para revisar mal etiquetado
  arrange(desc(tf_idf)) %>% 
  head() %>% 
  View()

token1_Freile = token1_Freile %>%
  filter(upos %in% c('NOUN','PROPN','VERB','ADJ')) %>% 
  bind_tf_idf(lemma, doc_id, n)

token1_Freile %>% 
  arrange(desc(tf_idf)) %>% 
  head() %>% 
  View()

# Visualización de tf-idf

token1_Velasco %>%
  filter(doc_id %in% unique(token1_Velasco$doc_id)[1:4]) %>% 
  arrange(desc(tf_idf)) %>%
  mutate(lemma = factor(lemma, levels = rev(unique(lemma)))) %>% # slice_max
  group_by(doc_id) %>% # slice_max
  top_n(15) %>% # slice_max
  ungroup() %>% # slice_max
  ggplot(aes(reorder_within(lemma, tf_idf, doc_id), tf_idf, fill = doc_id)) +
  geom_col(show.legend = FALSE) +
  scale_x_reordered()+
  labs(x = NULL, y = "tf-idf",
       title = "tf-idf: Cuatro noticias de Juan Fernando Velasco") +
  # facet_wrap(~doc_id, ncol = 2, scales = "free") + // lo mismo se consigue con vars()
  facet_wrap(vars(doc_id), ncol = 2, scales = "free") +
  coord_flip()

# Carga de dataframes originales, para tokenizar con ngrams

noticiasVelascoDF = readRDS("NoticiasDB/noticiasVelascoDF.RDS")
noticiasLarreaDF = readRDS("NoticiasDB/noticiasLarreaDF.RDS")
noticiasFreileDF = readRDS("NoticiasDB/noticiasFreileDF.RDS")

bigramas_Velasco = noticiasVelascoDF %>%
  unnest_tokens(bigrama, Noticia, token = "ngrams", n = 2)

bigramas_Velasco %>% 
  filter(Titular==ejemplo_Velasco) %>% 
  select(Titular, bigrama)

bigramas_Larrea = noticiasLarreaDF %>%
  unnest_tokens(bigrama, Noticia, token = "ngrams", n = 2)

bigramas_Larrea %>% 
  filter(Titular==ejemplo_Larrea) %>% 
  select(Titular, bigrama)

bigramas_Freile = noticiasFreileDF %>%
  unnest_tokens(bigrama, Noticia, token = "ngrams", n = 2)

bigramas_Freile %>% 
  filter(Titular==ejemplo_Freile) %>% 
  select(Titular, bigrama)

# Cargar stopwords
library(readxl)
stopwords_es_1 = read_excel("CustomStopWords.xlsx")
names(stopwords_es_1) = c("Token","Fuente") 
stopwords_es_2 = tibble(Token=tm::stopwords(kind = "es"), Fuente="tm")
stopwords_es = rbind(stopwords_es_1, stopwords_es_2)
stopwords_es = stopwords_es[!duplicated(stopwords_es$Token),]
remove(stopwords_es_1, stopwords_es_2)

# remover stopwords
bigramas_Velasco = bigramas_Velasco %>%
  separate(bigrama, c("palabra1", "palabra2"), sep = " ") %>%
  filter(!palabra1 %in% c(stopwords_es$Token)) %>%
  filter(!palabra2 %in% c(stopwords_es$Token))

bigramas_frec_Velasco = bigramas_Velasco %>% 
  count(Titular, palabra1, palabra2, sort = TRUE) %>% 
  unite(bigrama, palabra1, palabra2, sep = " ")

bigramas_frec_Velasco %>% select(bigrama, n) %>% head()

bigramas_Larrea = bigramas_Larrea %>%
  separate(bigrama, c("palabra1", "palabra2"), sep = " ") %>%
  filter(!palabra1 %in% c(stopwords_es$Token)) %>%
  filter(!palabra2 %in% c(stopwords_es$Token))

bigramas_frec_Larrea = bigramas_Larrea %>% 
  count(Titular, palabra1, palabra2, sort = TRUE) %>% 
  unite(bigrama, palabra1, palabra2, sep = " ")

bigramas_Freile = bigramas_Freile %>%
  separate(bigrama, c("palabra1", "palabra2"), sep = " ") %>%
  filter(!palabra1 %in% c(stopwords_es$Token)) %>%
  filter(!palabra2 %in% c(stopwords_es$Token))

bigramas_frec_Freile = bigramas_Freile %>% 
  count(Titular, palabra1, palabra2, sort = TRUE) %>% 
  unite(bigrama, palabra1, palabra2, sep = " ")

bigramas_frec_Freile %>% arrange(desc(n)) %>% head()


# Tf-Idf para bigramas
bigramas_tfidf_Velasco = bigramas_frec_Velasco %>%
  bind_tf_idf(bigrama, Titular, n)

bigramas_tfidf_Velasco %>% arrange(desc(tf_idf)) %>% head()

bigramas_tfidf_Larrea = bigramas_frec_Larrea %>%
  bind_tf_idf(bigrama, Titular, n)

bigramas_tfidf_Larrea %>% arrange(desc(tf_idf)) %>% head()

bigramas_tfidf_Freile = bigramas_frec_Freile %>%
  bind_tf_idf(bigrama, Titular, n)

bigramas_tfidf_Freile %>% arrange(desc(tf_idf)) %>% head()

# Grafos de red

library(igraph)

bigrama_grafo_Velasco = bigramas_Velasco %>%
  count(palabra1, palabra2, sort = TRUE) %>% 
  filter(n >= 6) %>%
  graph_from_data_frame()

bigrama_grafo_Velasco

bigrama_grafo_Larrea = bigramas_Larrea %>%
  count(palabra1, palabra2, sort = TRUE) %>% 
  filter(n >= 6) %>%
  graph_from_data_frame()

bigrama_grafo_Larrea

bigrama_grafo_Freile = bigramas_Freile %>%
  count(palabra1, palabra2, sort = TRUE) %>% 
  filter(n >= 6) %>%
  graph_from_data_frame()

bigrama_grafo_Freile

library(ggraph)
set.seed(123)

ggraph(bigrama_grafo_Velasco, layout = "fr") +
  geom_edge_link() +
  geom_node_point() +
  geom_node_label(aes(label = name), vjust = 1, hjust = 1)

ggraph(bigrama_grafo_Larrea, layout = "fr") +
  geom_edge_link() +
  geom_node_point() +
  geom_node_label(aes(label = name), vjust = 1, hjust = 1)

ggraph(bigrama_grafo_Freile, layout = "fr") +
  geom_edge_link() +
  geom_node_point() +
  geom_node_label(aes(label = name), vjust = 1, hjust = 1)

# Nube de palabras

library(echarts4r)
# library(stringi)
wc_Velasco = tidy_Velasco_annotated %>% 
  filter(upos %in% c("NOUN","PROPN", "ADJ", "VERB")) %>%
  count(lemma, sort=T) %>% 
  filter(n > 20) %>% 
  e_color_range(n, color) %>%
  e_charts() %>%
  e_cloud(lemma, n, color) %>% 
  e_tooltip()
wc_Velasco


# Palabras claves con algoritmo RAKE, agnostico al IDIOMA
library(udpipe)
Velasco_keywords = keywords_rake(x = tidy_Velasco, term = "lemma", group = "doc_id", relevant = tidy_Velasco$upos %in% c("NOUN", "ADJ","PROPN","VERB"), ngram_max = 3)
head(Velasco_keywords)
