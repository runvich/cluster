# Estimación ACP

## Gráfico de sedimentación

```{r}
screeplot(my.prc, main = "Gráfico de Sedimentación de notas", type = "line")
```

```{r, estimacion}
```

## Loadings

```{r, loadings}
```

```{r}
plot(order.loadings, main = myTitle, xlab = myXlab, cex = 1.5, col = "red")
```

## Biplot
```{r}
biplot(my.prc, cex = c(0.5, 0.6))
```

## Scores

```{r, echo = T}
scr <- my.prc2$scores
scr
```

### Score 1ra componente

```{r}
scr2 <- base[order(scr[, 1]), ]
DT::datatable(scr2)
```

Con relación a los scores obtenidos con anterioridad para todas las componentes. Las observaciones `19`, `16` y `12` son las generan mayor contribución a la primera componente.
